#!/usr/bin/clisp


(if (= (length *args*) 0)
  (error "usage: `<progname> <dices> <sides> <modifier>`, e.g. `<progname> 2 6 -3` to roll 2d6-3")
)


(setf *random-state* (make-random-state t))

(defun  roll-dice (sides)
	(+ 1 (random sides))
)



(defun do-roll (data)
  (+ (nth 2 data)
     (apply '+ (loop repeat (nth 0 data) collect (roll-dice (nth 1 data))))
  )
)

(defun make-roll-data (dices sides modifier)
	(list dices sides modifier)
)

(defun make-roll-data-from-string (str)
  (make-roll-data
	(parse-integer (nth 0 *args*))
	(parse-integer (nth 1 *args*))
	(parse-integer (nth 2 *args*))
  )
)


(format t "~%~a" (do-roll (make-roll-data-from-string *args*)))
